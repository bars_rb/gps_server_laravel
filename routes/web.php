<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'MainController@sales_reps')->name('sales_reps');
Route::get('/route/', 'MainController@route')->name('route');
Route::get('/route/{id}', 'MainController@route_for_user')->name('route_for_user');


Route::group(array('prefix' => 'rest'), function (){
    Route::get('/route/', 'RestController@route');
    Route::post('/correct/', 'RestController@correct');
    Route::post('/save_report/', 'RestController@save_report');
});



Route::group(['prefix' => 'api'], function()
{
    Route::post('/point', array('uses' => 'GPSController@saveGpsPoint'));
    Route::get('/message/{hardware_id}', array('uses' => 'GPSController@showMessage'));
    Route::get('/message/{hardware_id}/trackerVersion/{tracker_version}', array('uses' => 'GPSController@showMessage'));
    Route::get('/device_status/{hardware_id}', array('uses' => 'GPSController@deviceStatus'));
    Route::post('/register', array('uses' => 'GPSController@registerDevice'));
    Route::post('/logs', array('uses' => 'GPSController@saveLog'));
    Route::get('/update', array('uses' => 'GPSController@update'));

    Route::any('{catchall}', 'GPSController@fallback')
        ->where('catchall', '.*')
        ->fallback();


});


Route::group(array( 'prefix' => 'admin',
                    'middleware' => ['auth', 'roles'],
                    'roles' => ['admin']), function(){
    Route::get('/', ['uses' => 'Admin\IndexController@index',]);

    Route::get('/users', ['uses' => 'Admin\UserController@list',])->name('usersList');
    Route::get('/user/{id?}', 'Admin\UserController@showUserInfo')->name('userShow');
    Route::post('/user', 'Admin\UserController@saveUserInfo')->name('userSave');


    Route::get('/devices', ['uses' => 'Admin\DeviceController@list',])->name('devicesList');
    Route::get('/device/{id?}', 'Admin\DeviceController@showDeviceInfo')->name('deviceShow');
    Route::post('/device', 'Admin\DeviceController@saveDeviceInfo')->name('deviceSave');

    Route::get('/logs/{id?}', 'Admin\DeviceController@showLogs')->name('deviceLogs');

});

Route::get('/test', ['uses' => 'TestController@test',]);
Route::post('/test', ['uses' => 'TestController@test',]);