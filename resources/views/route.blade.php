@extends('layouts.app')

@section('content')
    <div class="container">
            <div id="result_modal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 id="modal_result_text" class="modal-title">Result text.</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row justify-content-center">

            <div class="col-md-4 centered">
                <h5 class="title">{{$sales_rep->name ?? "-"}}</h5>

                <div id="calendar-container" ></div>
                <a id="btnShowRoute" class="btn btn-primary btn-block btn-sm active" role="button" aria-pressed="true">Показать маршрут</a>

                <div class="card mt-2" style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">
                            Дата:
                            <span id="result_date">01.01.2001</span>
                            @if(!$auth_user->isUserOnly())
                                <span id="save_report"><i class="fas fa-save" style="color:#2176bd"></i></span>
                            @endif
                        </h5>
                        <h5 class="card-title">Результат:
                            <span id="full_distance">0</span> км.
                        </h5>
                        <h6 class="card-subtitle mb-2 text-muted">Расстояние: <span id="gps_distance">0</span> км.</h6>
                        @if(!$auth_user->isUserOnly())
                            <form class="mt-1">
                                <div class="row">
                                    <div class="col">
                                        <label for="distance_correct">Корректировка:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <input type="text" class="form-control form-control-sm" id="distance_correct" placeholder="0" value="">
                                    </div>
                                    <div class="col">
                                        <a id="btn_save_corection" class="btn btn-primary btn-block btn-sm active" role="button" aria-pressed="true">
                                            ОК</a>
                                    </div>

                                </div>

                            </form>
                        @endif
                        <p class="card-text">Посещение точек:</p>
                        <span id="pois_visit_result">
                            На эту дату точек нет.
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <script >
                    let sales_rep_id = {!! $sales_rep->id ?? "0" !!};
                    let date = ''+{!! $date ?? "20010101" !!};
                </script>
                <script src="http://api-maps.yandex.ru/2.1/?lang=ru-RU" type="text/javascript"></script>
                <div id="map" style="width:100%; height:100%"></div>
            </div>

        </div>
        <script src="{{ asset('js/route.js') }}" defer></script>
    </div>
@endsection
