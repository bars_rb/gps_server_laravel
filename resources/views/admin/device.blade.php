@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form method="post" action="{{route('deviceSave')}}">
                    {{ csrf_field() }}
                    <input id="id" name="id" type="hidden" value="{{$device->id}}">
                    <div class="form-group row">
                        <label for="title" class="col-4 col-form-label">Название</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">

                                </div>
                                <input id="title" name="title" placeholder="введите название" value="{{$device->title}}"
                                       type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4">Подтвеждение</label>
                        <div class="col-8">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input name="confirmed" id="confirmed" type="checkbox"
                                       {{ $device->is_confirmed}} class="custom-control-input" value="confirmed">
                                <label for="confirmed" class="custom-control-label">Подтвердить добавление этого
                                    устройства.</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4 col-form-label" for="hardware_id">ID устройства</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-fingerprint"></i>
                                    </div>
                                </div>
                                <input id="hardware_id" name="hardware_id" placeholder="1234567890123456"
                                       value="{{$device->hardware_id}}" type="text" class="form-control"
                                       aria-describedby="emailHelpBlock" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4 col-form-label" for="log_level">Уровень логирования</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="far fa-list-alt"></i>
                                    </div>
                                </div>
                                <select id="log_level" name="log_level" class="custom-select">
                                    <option value="">Выберите уровень логирования</option>
                                    @foreach($log_levels as $level_name => $level)
                                        <option value="{{$level}}"
                                                @if ($device->log_level==$level)
                                                selected
                                                @endif >
                                            {{$level_name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4 col-form-label" for="save_interval">Интвервал обновления местоположения</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="far fa-list-alt"></i>
                                    </div>
                                </div>
                                <select id="save_interval" name="save_interval" class="custom-select">
                                    <option value="" disabled>Выберите интервал</option>
                                        <option value="5"
                                                @if ($device->save_interval==5)
                                                selected
                                                @endif >
                                            5 сек / 50 метров
                                        </option>
                                    <option value="10"
                                            @if ($device->save_interval==10)
                                            selected
                                            @endif >
                                        10 сек / 100 метров
                                    </option>
                                    <option value="15"
                                            @if ($device->save_interval==15)
                                            selected
                                            @endif >
                                        15 сек / 150 метров
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="user" class="col-4 col-form-label">Пользователь</label>
                        <div class="col-8">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-address-card"></i>
                                    </div>
                                </div>
                                <select id="user" name="user" class="custom-select">
                                    <option value="">Выберите пользователя</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}"
                                                @if ($device->user_id==$user->id)
                                                selected
                                                @endif >
                                            {{$user->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="message" class="col-4 col-form-label">Сообщение</label>
                        <div class="col-8">
                            <textarea id="message" name="message" cols="40" rows="5"
                                      class="form-control">{{$device->message}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-4 col-8">
                            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
