@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Пользователи:</h2>
            <div class="list-group">
                @foreach($users as $user)
                        <a href = "{{ route('userShow', ['id'=>$user->id]) }}" class="list-group-item list-group-item-action p-2">{{ $user->name }}</a>
                @endforeach
            </div>
        </div>
        <a href = {{ route('userShow') }}>Добавить</a>
    </div>
</div>
@endsection
