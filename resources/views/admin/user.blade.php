@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form method="post" action="{{route('userSave')}}">
                {{ csrf_field() }}
                <input id="id" name="id" type="hidden" value="{{$user->id}}">
                <div class="form-group row">
                    <label for="name" class="col-4 col-form-label">Имя</label>
                    <div class="col-8">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-address-card"></i>
                                </div>
                            </div>
                            <input id="name" name="name" placeholder="введите имя" value="{{$user->name}}" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-4 col-form-label" for="email">Почтовый адрес</label>
                    <div class="col-8">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-at"></i>
                                </div>
                            </div>
                            <input id="email" name="email" placeholder="email@parfumklas.ru" value="{{$user->email}}" type="text" class="form-control" aria-describedby="emailHelpBlock" required="required">
                        </div>
                        <span id="emailHelpBlock" class="form-text text-muted">Почта используется как логин</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-4 col-form-label">Пароль</label>
                    <div class="col-8">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-asterisk"></i>
                                </div>
                            </div>
                            <input id="password" name="password" placeholder="Введите пароль" type="password" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-4">Супервайзер</label>
                    <div class="col-8">
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input name="is_supervisor" id="is_supervisor" type="checkbox" {{ $user->is_supervisor}} class="custom-control-input" value="is_supervisor">
                            <label for="is_supervisor" class="custom-control-label">Является супервайзером</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="id_1c" class="col-4 col-form-label">Код в 1С</label>
                    <div class="col-8">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                            </div>
                            <input id="id_1c" name="id_1c" placeholder="УТ0001" value="{{$user->id_1c}}" type="text" class="form-control">

                            <label for="select_supervisor" class="col-4 col-form-label">Супервайзер</label>
                            <select id="select_supervisor" name="select_supervisor" class="custom-select">
                                <option value="" disabled selected>Выберите супервайзера</option>
                                @foreach($supervisors as $supervisor)
                                    <option value="{{$supervisor->id}}"
                                            @if ($user->supervisor_id==$supervisor->id)
                                                selected
                                            @endif >
                                        {{$supervisor->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-4 col-8">
                        <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
