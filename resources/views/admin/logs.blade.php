@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md">
                <h2>Логи устройства <b>{{$device->title}}</b>:</h2>
                <table class="table table-striped table-bordered table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Timestamp</th>
                        <th scope="col">Level</th>
                        <th scope="col">Tag</th>
                        <th scope="col">Message</th>
                        <th scope="col">Info</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($logs as $log)
                        <tr>
                            <td scope="col">{{$log->timestamp}}</td>
                            <td scope="col">{{$log->level}}</td>
                            <td scope="col">{{$log->tag}}</td>
                            <td scope="col">{{$log->message}}</td>
                            <td scope="col">{{$log->senderName}}/{{$log->osVersion}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
