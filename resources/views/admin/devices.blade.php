@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Устройства:</h2>
                <table class="table table-striped table-bordered table-hover table-sm">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Устройство</th>
                        <th scope="col">Версия трекера</th>
                        <th scope="col">Последнее соединение</th>
                        <th scope="col">Логи</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($devices as $device)
                        <tr scope="row">
                            <td><a href = {{ route('deviceShow', ['id'=>$device->id]) }}>{{ $device->title }}</a></td>
                            <td>{{$device->tracker_version}}</td>
                            <td>{{$device->last_visit}}</td>
                            <td><a href = {{ route('deviceLogs', ['id'=>$device->id]) }}><img src="/images/log-icon.png" alt="Log"/></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <a href = {{ route('deviceShow') }}>Добавить</a>
        </div>
    </div>
@endsection
