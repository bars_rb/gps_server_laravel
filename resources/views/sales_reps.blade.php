@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="list-group">
                    @foreach($sales_reps as $sales_rep)
                            <a href="{{ route('route_for_user', ['id'=>$sales_rep->id]) }}" class="list-group-item list-group-item-action p-2">{{ $sales_rep->name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
