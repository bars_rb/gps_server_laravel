<?php

namespace App\Http\Controllers;

use App\Correction;
use App\GpsPoint;
use App\POI;
use App\Services\RouteReportService;
use App\Services\SaveReportsService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RestController extends Controller
{
    public function correct(Request $request){
        if(Auth::user()==null){
            return response()->json([
                'message' => 'Not authorised.'
            ], 403);
        }

        if(Auth::user()->isUserOnly()){
            $response = ['result' => 'error',
                         'message' => 'You have no power here!'];
            return response()->json($response, 200);
        }

        $correction = Correction::where([
            ['user_id', '=', $request->sales_rep_id],
            ['date', '=', $request->date]
        ])->first();
        if($correction==null){
            $correction = new Correction();
            $correction->user_id = $request->sales_rep_id;
            $correction->date = $request->date;
        }
        if(is_numeric($request->distance)){
            $correction->distance = $request->distance;
            $correction->save();

            return response()->json($this->statistic($request->sales_rep_id, $request->date), 200);
        }
        else{
            if(strlen($request->distance)==0){
                $correction->delete();
                return response()->json($this->statistic($request->sales_rep_id, $request->date), 200);
            }
            else{
                return response()->json([
                    'result' => 'Distance wrong!'], 200);
            }
        }
    }

    public function route(Request $request){
        if(Auth::user()==null){
            return response()->json([
                'message' => 'Not authorised.'
            ], 401);
        }

        $user = User::find($request->sales_rep_id);

        if(Auth::user()->isUserOnly() && $user->id != Auth::user()->id){
            $response = ['result' => 'error',
                         'message' => 'You have no power here!'];
            return response()->json($response, 200);
        }

        $points = GpsPoint::where([
            ['user_id', '=', $request->sales_rep_id],
            ['date', '=', $request->date]
        ])->get();

        $POIs = POI::where([
            ['user_id', '=', $request->sales_rep_id],
            ['date', '=', $request->date]
        ])->get();

        $route_stat = $this->statistic($request->sales_rep_id, $request->date, $POIs);
        $map_route = RouteReportService::generateMapRoute($points);
        $map_pois = RouteReportService::generateMapPOIs($POIs);

        $response = $route_stat;
        $response['map_route'] = $map_route;
        $response['map_pois'] = $map_pois;
        $response['pois'] = $POIs;
        return response()->json($response, 200);
    }


    public function save_report(Request $request){
        if(Auth::user()==null){
            return response()->json([
                'message' => 'Not authorised.'
            ], 403);
        }
        $user = User::find($request->sales_rep_id);
        if(Auth::user()->isUserOnly()){
            $response = ['result' => 'error',
                         'message' => 'You have no power here!'];
        }
        else if($user!=null){
            SaveReportsService::saveReportForUserForDate($user, $request->date);
            $response = ['result' => 'ok',];
        }
        else{
            $response = ['result' => 'error',
                         'message' => 'User error.'];
        }
        return response()->json($response, 200);
    }


    private function statistic($sales_rep_id, $date, $POIs = null){
        $points = GpsPoint::where([
            ['user_id', '=', $sales_rep_id],
            ['date', '=', $date]
        ])->get();

        $route_stat = RouteReportService::getStat($points, $POIs);
        $first_point_time = $this->formatTime($route_stat['first_point_time']);
        $last_point_time = $this->formatTime($route_stat['last_point_time']);


        $correction = Correction::where([
            ['user_id', '=', $sales_rep_id],
            ['date', '=', $date]
        ])->first();

        $correction_distance = ($correction != null) ? $correction->distance : "";

        return [
            'result' => 'ok',
            'distance' => round($route_stat['distance'] / 1000, 2),
            'correction' => $correction_distance,
            'min_lat' => $route_stat['min_lat'],
            'min_lon' => $route_stat['min_lon'],
            'max_lat' => $route_stat['max_lat'],
            'max_lon' => $route_stat['max_lon'],
            'first_point_time' => $first_point_time,
            'last_point_time' => $last_point_time,
        ];
    }

    private function formatTime($time)
    {
        return substr($time, 0, 2).":".substr($time, 2, 2).":".substr($time, 4, 2);
    }
}
