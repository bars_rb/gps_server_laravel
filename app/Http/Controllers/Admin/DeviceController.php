<?php

namespace App\Http\Controllers\Admin;

use App\Device;
use App\Log;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeviceController extends Controller
{

    public function list(){
        $devices = Device::all();

        $data =[
            'devices'=> $devices
        ];
        return view('admin.devices', $data);
    }

    public function showDeviceInfo($id = 0) {
        if($id!=0){
            $device = Device::find($id);
            $device->getRelations();
        }
        else{
            $device = new Device();
            $device->id = 0;
        }
        $device->user;

        if($device->user!=null){
            $device->user_id=$device->user->id;
        }

        $device->is_confirmed = $device->confirmed?" checked ":"";

        $users = User::all();

        $log_levels = $this->getLogLevels();
        $data = [
            'device' => $device,
            'users'=> $users,
            'log_levels' => $log_levels,
        ];
        return view('admin.device', $data);
    }

    public function showLogs($id = 0) {
        $logs = Log::where('device_id', $id)
                    ->orderBy('timestamp', 'desc')
                    ->take(30)
                    ->get();

        $device = Device::find($id);

        $data = ['device' => $device,
            'logs'=> $logs];
        return view('admin.logs', $data);

        //return redirect(route('devicesList'));
    }

    public function saveDeviceInfo(Request $request) {

        if($request->id!=0) {
            $device = Device::find($request->id);
            $device->getRelations();
        }
        else{
            $device = new Device();
        }

        $device->title = $request->title;
        $device->hardware_id = $request->hardware_id;
        $device->log_level = $request->log_level;
        $device->save_interval = $request->save_interval;
        $device->message = isset($request->message)?$request->message:"";

        $device->confirmed = ($request['confirmed'] =="confirmed") ;

        if($request->user!=null){
            $user = User::find($request['user']);
            if($user!=null){
                $device->user()->dissociate();
                $device->user()->associate($user);
            }
        }
        else{
            $device->user()->dissociate();
        }


        $device->save();

        echo('Данные сохранены!');
        return redirect(route('devicesList'));
    }

    private function getLogLevels()
    {
        return [
            'VERBOSE'=>2,
            'DEBUG'=>3,
            'INFO'=>4,
            'WARN'=>5,
            'ERROR'=>6,
            'ASSERT'=>7,
        ];
    }
}
