<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function list(){
        $users = User::all();

        $data =[
            'users'=> $users
        ];
        return view('admin.users', $data);
    }

    public function showUserInfo($id = 0) {
        if($id!=0){
            $user = User::find($id);
            $user->getRelations();
        }
        else{
            $user = new User();
            $user->id = 0;
        }
        $user->roles;
        if($user->supervisor->first()!=null){
            $user->supervisor_id=$user->supervisor->first()->id;
        }

        $supervisors = Role::where('name', 'supervisor')->first()->users;


        if($user->hasRole('supervisor')){
            $user->is_supervisor = " checked ";
        }
        else
        {
            $user->is_supervisor = "";
        }

        $data = ['user' => $user,
            'supervisors'=> $supervisors];
        return view('admin.user', $data);
    }

    public function saveUserInfo(Request $request) {

        if($request->id!=0) {
            $user = User::find($request->id);
            $user->getRelations();
        }
        else{
            $user = new User();
        }

        $user->name = $request->name;
        $user->email = $request->email;
        if(strlen($request->password)!=0) {
            $user->password = bcrypt($request->password);
        }

        $user->id_1c = $request->id_1c;
        $user->save();

        $supervisor = User::where('id', $request['select_supervisor'])->first();
        if($supervisor!=null){
            $user->supervisor()->detach();
            $user->supervisor()->attach($supervisor);
        }


        $user_role = Role::where('name', 'user')->first();
        $supervisor_role = Role::where('name', 'supervisor')->first();

        if ($request["is_supervisor"] == "is_supervisor") {
            if(!$user->roles()->where('name', 'supervisor')->exists()) {
                $user->roles()->attach($supervisor_role);
                $user->supervisor()->detach();
                $user->roles()->detach($user_role);
            }
        } else {
            $user->roles()->attach($user_role);
            $user->roles()->detach($supervisor_role);
        }
        echo('Данные сохранены!');
        return redirect(route('usersList'));
    }
}
