<?php

namespace App\Http\Controllers;

use App\Device;
use App\GpsPoint;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class GPSController extends Controller
{
    public function saveGpsPoint(Request $request)
    {
        $device = Device::where('hardware_id', $request->hardware_id)->first();
        if ($device == null) {
            return response()->json([
                'message' => 'device not found'
            ], 403);
        }

        $user = $device->user;
        if ($user == null) {
            return response()->json([
                'message' => 'Device not attached to user'
            ], 403);
        }

        $validator = Validator::make($request->all(), [
            'lat' => 'required|min:2',
            'lon' => 'required|min:2',
            'date' => 'required|size:8',
            'time' => 'required|size:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Data error'
            ], 403);
        }

        $point = new GpsPoint();
        $point->user_id = $user->id;
        $point->lat = $request->lat;
        $point->lon = $request->lon;
        $point->date = $request->date;
        $point->time = $request->time;
        try {
            $point->save();
        } catch(\Illuminate\Database\QueryException $ex){
            if($ex->getCode()=="23000"){
                return response()->json(null, 200);
            }
            else{
                return response()->json([
                    'message' => 'Data error'
                ], 403);
            }
        }
        return response()->json(null, 200);
    }

    public function showMessage($hardware_id = null, $tracker_version = null)
    {
        $device = Device::where('hardware_id', $hardware_id)->first();
        if ($device == null) {
            return response()->json([
                'message' => 'Device not registered!'
            ], 403);
        } else if (!$device->confirmed) {
            return response()->json([
                'message' => 'Ожидайте подтверждения. Спасибо.'
            ], 403);
        }

        $message = $device->message;

        if ($tracker_version != null) {
            $device->tracker_version = $tracker_version;
            if($tracker_version<"20190514"){
                $message = "Необходимо обновить программу! Нажмите +, 'Настройки', 'Обновить'.";
            }
        }

        $device->last_visit=now();
        $device->save();

        return response()->json([
            'message' => $message,
            'current_version' => 20190514,
            'log_level' => $device->log_level,
            'save_interval' => $device->save_interval,
        ], 200);
    }

    public function registerDevice(Request $request)
    {
        $device = Device::where('hardware_id', $request->hardware_id)->first();
        if ($device == null) {
            $device = new Device();
            if ($request->name != null) {
                $device->title = date("d.m.Y") . " - " . $request->name;
            } else {
                $device->title = date("d.m.Y") . " - " . $request->hardware_id;
            }
            $device->hardware_id = $request->hardware_id;
            $device->save();
            return response()->json([
                'message' => 'Device created. Wait for confirm!',
                'result' => 'created'
            ], 200);
        } else if (!$device->confirmed) {
            return response()->json([
                'message' => 'Device not confirmed. Wait for confirm!',
                'result' => 'not_confirmed'
            ], 200);
        }

        return response()->json([
            'message' => "Device already registered!",
            'result' => 'already_registered'
        ], 200);
    }

    public function deviceStatus(Request $request)
    {
        $device = Device::where('hardware_id', $request->hardware_id)->first();
        if ($device == null) {
            $message = 'not_registered';
            $status = 403;
        } else if (!$device->confirmed) {
            $message = 'not_confirmed';
            $status = 403;
        } else {
            $message = 'registered';
            $status = 200;
        }

        return response()->json([
            'message' => $message,
        ], $status);
    }

    public function saveLog(Request $request)
    {
        $device = Device::where('hardware_id', $request->deviceUUID)->first();
        if ($device == null) {
            return response()->json([
                'message' => 'device not found '. $request->deviceUUID
            ], 403);
        }

        $log = new Log();
        $log->device_id = $device->id;
        $log->level = $request->level;
        $log->tag = $request->tag;
        $log->message = $request->message;
        $timestamp = $request->timestamp;
        if(strpos($timestamp, "Z")){
            $timestamp = str_replace("Z", "", $timestamp);
            $timestamp = str_replace("T", "T", $timestamp);
        }
        $log->timestamp = $timestamp;
        $log->senderName = $request->senderName;
        $log->osVersion = $request->osVersion;
        $log->save();

        return response()->json([
            'message' => "ok"
        ], 200);
    }

    public function update(Request $request){
        $pathToFile = "updates/gps_tracker.apk";
        return Storage::download($pathToFile);
    }

    public function fallback(Request $request){
        return response()->json([
            'message' => 'Page not found'
        ], 404);
    }

}
