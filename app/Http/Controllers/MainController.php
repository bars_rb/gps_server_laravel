<?php

namespace App\Http\Controllers;

use App\Correction;
use App\GpsPoint;
use App\POI;
use App\Role;
use App\Services\RouteReportService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sales_reps()
    {
        $auth_user = Auth::user();
        $auth_user->roles;
        if ($auth_user->isUserOnly()) {
            return redirect(route('route'));
        }

        if ($auth_user->hasRole('admin')) {
            $sales_reps = User::all();
        } else {
            $sales_reps = $auth_user->sales_reps;
        }
        $data = [
            'sales_reps' => $sales_reps,
            'auth_user' => $auth_user,
        ];

        //dd(get_defined_vars());
        return view('sales_reps', $data);
    }

    public function route(Request $request)
    {
        $user = Auth::user();
        $user->roles;
        if (!$user->isUserOnly()) {
            return redirect(route('sales_reps'));
        } else {
            return ($this->route_for_user($request, $user->id));
        }
    }

    public function route_for_user(Request $request, $sales_rep_id = null)
    {
        $auth_user = Auth::user();
        $auth_user->roles;
        if ($auth_user->isUserOnly() && $sales_rep_id != $auth_user->id) {
            return redirect(route('route'));
        }

        $sales_rep = User::find($sales_rep_id);

        if($request->date==null){
            $date = date("Ymd");
        }
        else{
            $date=$request->date;
        }


        $data = [
            'auth_user' => $auth_user,
            'sales_rep' => $sales_rep,
            'date' => $date,
        ];

        //dd(get_defined_vars());
        return view('route', $data);
    }

    public function index()
    {
        $user = Auth::user();
        $user->roles;
        if ($user->isUserOnly()) {
            return redirect(route('route'));
        } else {
            return redirect(route('sales_reps'));
        }
    }


}
