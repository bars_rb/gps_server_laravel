<?php


namespace App\Services;

use App\Correction;
use App\GpsPoint;
use App\POI;
use App\User;

class SaveReportsService
{
    public function __invoke()
    {
        $yesterday = mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"));
        $date = date("Ymd", $yesterday);

        $this->saveReportForAllUsersForDate($date);
    }

    public function saveReportForAllUsersForDate($date){
        foreach (User::all() as $sales_rep) {
            SaveReportsService::saveReportForUserForDate($sales_rep, $date);
        }
    }

    public static function saveReportForUserForDate($sales_rep, $date)
    {
        $root_path = env('SCRIPT_PATH');
        $dir_path = $root_path."/public/share/reports/";

        $points = GpsPoint::where([
            ['user_id', '=', $sales_rep->id],
            ['date', '=', $date]
        ])->get();

        $POIs = POI::where([
            ['user_id', '=', $sales_rep->id],
            ['date', '=', $date]
        ])->get();

        $correction = Correction::where([
            ['user_id', '=', $sales_rep->id],
            ['date', '=', $date]
        ])->first();

        $route_stat = RouteReportService::getStat($points, $POIs);
        if ($route_stat['distance'] == 0) {
            return;
        }
        $correction_distance = ($correction != null) ? $correction->distance : 0;
        $full_distance = round(($route_stat['distance'] / 1000) + $correction_distance, 2);

        $report = new \SimpleXMLElement('<report/>');
        $report->addChild('sale_rep', $sales_rep->id_1c);
        $report->addChild('date', $date);
        $report->addChild('distance', $full_distance);

        $pois = $report->addChild('points');

        foreach ($POIs as $poi) {
            $point = $pois->addChild('point');
            $point->addChild('route', $poi->route_1c);
            $point->addChild('id', $poi->poi_1c);
            if ($poi->visited) {
                $point->addChild('visited', "true");
                $point->addChild('time', $poi->time);
            } else {
                $point->addChild('visited', "false");
            }
        }

        $export_filename = $dir_path . $sales_rep->id . "_" . $date . ".xml";
        $report->asXml($export_filename);
    }


}