<?php


namespace App\Services;

use App\GpsPoint;
use Illuminate\Support\Facades\Mail;

class CleanDBService
{
    public function __invoke()
    {
        $latest_date = date("Ymd", strtotime("-3 month"));

        $deletedRows = GpsPoint::where('date', '<' , $latest_date)->delete();
        $text = "DB cleaned. Removed ".$deletedRows." rows, older then ".date("d.m.Y", strtotime("-3 month"));
        Mail::raw($text, function ($message){
            $message->from('report@klas.aromata.ru', 'GPS tracker server');
            $message->subject("Очистка базы GPS-трекера");
            $message->to('top@klas.aromata.ru');
        });
    }


}