<?php


namespace App\Services;

use GuzzleHttp\Client;

use App\POI;
use Illuminate\Foundation\Auth\User;

class ImportPOIs
{
    public function __invoke()
    {
        $root_path = env('SCRIPT_PATH');
        $dir_path = $root_path."/public/share/routes_in";
        if ($list = scandir($dir_path)) {
            foreach ($list as $file) {
                if ($file != "." && $file != ".." && is_file($dir_path . "/" . $file)) {
                    if($this->add_route_from_file($dir_path . "/" . $file)){
                        unlink($dir_path . "/" . $file);
                    }
                    else{
                        $new_file = date("Y-m-d_H.i.s") . "_" . time() . "_" . $file;
                        rename($dir_path . "/" . $file, $dir_path ."/old/$new_file");
                    }

                }
            }
        }
    }

    private function add_route_from_file($file_path)
    {
        libxml_use_internal_errors(true);
        if (!$route = simplexml_load_file($file_path))
            return false;

        $sales_rep = User::where('id_1c', (string)$route->sales_rep)->first();
        if ($sales_rep == null) {
            return false;
        }

        $route_1c = (string)$route->route_id;

        foreach ($route->points->point as $point) {
            $title = (string)$point->title;
            $address = (string)$point->address;
            $poi_1c = (string)$point->poi_1c;
            $date = (string)$point->date;

            $poi = POI::where([
                ['route_1c', '=', $route_1c],
                ['poi_1c', '=', $poi_1c],
            ])->first();

            if($poi==null){
                $poi = new POI();
                $poi->user_id = $sales_rep->id;
                $poi->route_1c = $route_1c;
                $poi->poi_1c = $poi_1c;
                $poi->address = $address;
                $poi->title = $title;
                if($title==null || strlen($title)==0)
                {
                    $poi->title = $address;
                }
                $pos = $this->geolocate_address($address);
                if($pos!=null){
                    $coord = explode(" ",$pos);
                    $poi->lat = $coord[1];
                    $poi->lon = $coord[0];
                }

            }

            $poi->date = $date;

            if( $poi->lat == null ||
                $poi->lon == null ||
                strlen($poi->lat) == 0 ||
                strlen($poi->lon) == 0 )
            {
                continue;
            }
            else{
                $poi->save();
            }
        }

        return true;
    }

    private function geolocate_address($address)
    {
        $geoloc = "http://geocode-maps.yandex.ru/1.x/?geocode=" . urlencode($address); //$city+$street+$house";
        $client = new Client(['verify' => false]);
        $res = $client->request('GET', $geoloc);
        $response = simplexml_load_string($res->getBody());
        $geoObject = $response->GeoObjectCollection->featureMember[0];
        if($geoObject!=null){
                return($geoObject->GeoObject->Point->pos);
        }
        else {
            return null;
        }

    }
}