<?php


namespace App\Services;

define('EARTH_RADIUS', 6372795);

class RouteReportService
{

    public static function getStat($points, $POIs)
    {
        $distance = 0;
        $map_points = [];
        $min_lat = null;
        $max_lat = null;
        $min_lon = null;
        $max_lon = null;
        $first_point_time = "000000";
        $last_point_time = "000000";

        if (count($points) != 0) {
            $prev_lat = $points[0]->lat;
            $prev_lon = $points[0]->lon;
            $min_lat = $prev_lat;
            $max_lat = $prev_lat;
            $min_lon = $prev_lon;
            $max_lon = $prev_lon;
            $first_point_time = $points[0]->time;

            foreach ($points as $point) {
                $distance += RouteReportService::calculateTheDistance($prev_lat, $prev_lon, $point->lat, $point->lon);
                $prev_lat = $point->lat;
                $prev_lon = $point->lon;
                $last_point_time = $point->time;
                $map_points[] = [floatval ($point->lat),floatval ($point->lon)];
                if($POIs!=null) {
                    RouteReportService::checkPOIs($point, $POIs);
                }

                if ($prev_lat > $max_lat) $max_lat = $prev_lat;
                if ($prev_lat < $min_lat) $min_lat = $prev_lat;
                if ($prev_lon > $max_lon) $max_lon = $prev_lon;
                if ($prev_lon < $min_lon) $min_lon = $prev_lon;
            }
        }

        return [
            'min_lat' => $min_lat,
            'min_lon' => $min_lon,
            'max_lat' => $max_lat,
            'max_lon' => $max_lon,
            'distance' => $distance,
            'first_point_time' => $first_point_time,
            'last_point_time' => $last_point_time,
        ];
    }

    public static function generateMapRoute($points)
    {
        $map_points = [];
        foreach ($points as $point) {
            $map_points[] = [floatval ($point->lat),floatval ($point->lon)];
        }
        return $map_points;
    }

    public static function generateMapPOIs($POIs)
    {
        $map_pois= [];
        foreach ($POIs as $poi) {
            if ($poi->visited) {
                $map_pois[] = RouteReportService::make_poi($poi->lat, $poi->lon, 100, $poi->title, "00FF0077", "00AA00", $poi->time);
            } else {
                $map_pois[] = RouteReportService::make_poi($poi->lat, $poi->lon, 100, $poi->title, "FF000077", "AA0000","");
                $poi->time = "--:--:--";
            }
        }
        return $map_pois;
    }

    private static function calculateTheDistance($A1, $A2, $B1, $B2)
    {
        // перевести координаты в радианы
        $lat1 = $A1 * M_PI / 180;
        $lat2 = $B1 * M_PI / 180;
        $long1 = $A2 * M_PI / 180;
        $long2 = $B2 * M_PI / 180;

        // косинусы и синусы широт и разницы долгот
        $cl1 = cos($lat1);
        $cl2 = cos($lat2);
        $sl1 = sin($lat1);
        $sl2 = sin($lat2);
        $delta = $long2 - $long1;
        $cdelta = cos($delta);
        $sdelta = sin($delta);

        // вычисления длины большого круга
        $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
        $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

        //
        $ad = atan2($y, $x);
        $dist = $ad * EARTH_RADIUS;
        return $dist;
    }

    private static function checkPOIs($point, $POIs)
    {
        foreach ($POIs as $poi) {
            if ($poi->visited) {
                continue;
            } else {
                $dist = RouteReportService::calculateTheDistance($poi->lat, $poi->lon, $point->lat, $point->lon);
                if ($dist <= 100) {
                    $poi->visited = true;
                    $poi->time = RouteReportService::formatTime($point->time);
                }
            }
        }
    }

    private static  function formatTime($time){
        $hour = substr($time, 0, 2);
        $min = substr($time, 2, 2);
        $sec = substr($time, 4, 2);
        return($hour . ":" . $min . ":" . $sec);
    }

    private static function make_poi($lat, $lon, $radius, $title, $fillColor, $strokeColor, $time)
    {
        return "var poi = new ymaps.Circle([[$lat, $lon],$radius],{hintContent: \"$title $time\"},".
            "{fillColor: \"#$fillColor\",strokeColor: \"#$strokeColor\",strokeOpacity: 0.8,strokeWidth: 2});";
    }

}