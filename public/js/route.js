let myMap;
// let date;
$(document).ready(function () {
    $('#calendar-container').datepicker({
        format: "yymmdd",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "ru",
        todayHighlight: true
    });

    $('#calendar-container').data('datepicker').setDate(date.substr(0,4)+"/"+date.substr(4,2)+"/"+date.substr(6,2));

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    $('#btnShowRoute').click(get_route);

    $('#btn_save_corection').click(save_correcting);

    $('#save_report').click(save_report);

    ymaps.ready(init);



});

function get_route() {
    let calendar_date = $('#calendar-container').data('datepicker').getDate();
    if(calendar_date!=null){
        date = $.datepicker.formatDate("yymmdd", calendar_date);
    }
    date = ''+date;
    // let formated_date = $.datepicker.formatDate("yymmdd", calendar_date);
    let post_data = {'sales_rep_id': sales_rep_id, 'date': date};
    $.get("/rest/route", post_data)
        .done(function (response) {
             if (response.result == "ok") {
                 update_statsic_and_map(response);
            };
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            if(jqXHR.status === 0){
                alert( "Network error." );
            }
        });
}

function save_correcting(){
    let distance = $('#distance_correct').val();
    let post_data = {'sales_rep_id': sales_rep_id, 'date': date, 'distance': distance};
    $.post("/rest/correct", post_data).done(function (response) {
        if (response.result == "ok") {
            $("#gps_distance").text(response.distance);
            $("#distance_correct").val(response.correction);
            $("#full_distance").text(0 + response.distance + response.correction);
            $('#modal_result_text').text("Корректировка сохранена.");
            $('#result_modal').modal('show');
        }
        else {
            alert(response.result);
        }
    });
}

function save_report(){
    let post_data = {'sales_rep_id': sales_rep_id, 'date': date};
    $.post("/rest/save_report", post_data).done(function (response) {
        if (response.result == "ok") {
            $('#modal_result_text').text("Данные выгружены.");
            $('#result_modal').modal('show');
        }
        else {
            $('#modal_result_text').text(response.message);
            $('#result_modal').modal('show');
        }
    });
}

function init() {
    myMap = new ymaps.Map('map', {
        center: [55.76, 37.64],
        zoom: 11,
        controls: ['zoomControl', 'searchControl', 'typeSelector', 'fullscreenControl', 'rulerControl']
    });
    get_route()
}


function update_statsic_and_map(response) {
    $("#result_date").text(date.substr(6,2)+"."+date.substr(4,2)+"."+date.substr(0,4));
    $("#gps_distance").text(response.distance);
    $("#distance_correct").val(response.correction);
    $("#full_distance").text(0 + response.distance + response.correction);
    myMap.geoObjects.removeAll();

    properties = {
        hintContent: response.first_point_time + " - " + response.last_point_time
    };
    options = {
        strokeColor: '#ff0000',
        strokeWidth: 2
    };
    route = new ymaps.Polyline(response.map_route, properties, options);

    myMap.geoObjects.add(route);

    response.map_pois.forEach(function (poi_) {
        eval(poi_);
        myMap.geoObjects.add(poi);
    });

    if(response.pois.length > 0){
        $("#pois_visit_result").text("");

        response.pois.forEach(function (poi) {
            if (poi.visited){
                $("#pois_visit_result").append("<i class=\"far fa-check-square\" style=\"color: #00AA00;\"></i> ");
            }
            else{
                $("#pois_visit_result").append("<i class=\"fas fa-times\" style=\"color: #AA0000;\"></i> ");
            }
            $("#pois_visit_result").append(poi.title+" - "+poi.time+"<br>");
        });
    }
    else{
        $("#pois_visit_result").text("На эту дату точек нет.");
    }

    if(response.min_lat!=null){
        myMap.setBounds([[response.min_lat, response.min_lon], [response.max_lat, response.max_lon]],
            [$('#map').width(), $('#map').height()]);
    }
    else {
        myMap.setCenter([55.76, 37.64], 11);
    }
}
