<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->default("unknown");
            $table->integer('user_id')->unsigned()->index()->default(0);
            $table->string('hardware_id')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->string('message')->default("");;
            $table->string('tracker_version')->nullable();
            $table->string('scanner_version')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
