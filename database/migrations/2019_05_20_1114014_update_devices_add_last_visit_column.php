<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDevicesAddLastVisitColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `devices` DROP COLUMN  `created_at`;");
        DB::statement("ALTER TABLE `devices` DROP COLUMN  `updated_at`;");
        DB::statement("ALTER TABLE `devices` ADD `last_visit` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;");

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `devices` ADD `created_at` TIMESTAMP;");
        DB::statement("ALTER TABLE `devices` ADD `updated_at` TIMESTAMP;");
        DB::statement("ALTER TABLE `devices` DROP COLUMN  `last_visit`;");
    }
}
