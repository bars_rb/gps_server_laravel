<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where("name", 'admin')->first();
        $role_user = Role::where("name", 'user')->first();

        $employee = new User();
        $employee->name = "Admin";
        $employee->email = "admin@example.com";
        $employee->password = bcrypt("secret");
        $employee->save();
        $employee->roles()->attach($role_admin);
        $employee->roles()->attach($role_user);
    }
}
